package com.vytrykhovskyi;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TaskTwo {
    private Scanner scanner = new Scanner(System.in);

    private void someAction() {
        Map<String, Command> commands = new HashMap<>(); //lambda

        commands.put("Lambda", string -> System.out.println("Doing with lambda " + string));

        commands.put("Ref", this::methodReference);
        commands.put("New", new Command() {
            @Override
            public void run(String string) {
                System.out.println("Doing with anonymous class " + string);
            }
        });

        ClassCommand classCommand = new ClassCommand("Object");
        commands.put("Object", classCommand);

        System.out.println("Available commands: Lambda, Ref, New, Object");

        System.out.println("Input command:");
        String command = scanner.next();

        System.out.println("Input line:");
        String line = scanner.next();

        commands.get(command).run(line);


    }

    private void methodReference(String s) {
        System.out.println("Doing with method ref " + s);
    }

    public static void main(String[] args) {
        new TaskTwo().someAction();
    }
}
