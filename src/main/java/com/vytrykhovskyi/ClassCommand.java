package com.vytrykhovskyi;

public class ClassCommand implements Command {

    private String string;

    ClassCommand(String string) {
        this.string = string;
    }

    @Override
    public void run(String string) {
        System.out.println("Doing with new object of interface Command " + string);
    }
}
