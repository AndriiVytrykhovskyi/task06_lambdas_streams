package com.vytrykhovskyi;

import java.util.DoubleSummaryStatistics;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskThree {

    private static List<Integer> createListUsingThreadLocalRandom(int count) {
        return ThreadLocalRandom.current()
                .ints(count)
                .boxed()
                .collect(Collectors.toList());

    }

    private static List<Integer> createListUsingRandom(int start, int end, int count) {
        int bound = end - start + 1;
        Random random = new Random();
        return Stream.iterate(random.nextInt(bound) + start, n -> random.nextInt(bound) + start)
                .limit(count)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Integer> list = createListUsingRandom(0, 100, 11);

        System.out.println("Massive:");
        System.out.println(list);

        System.out.println("Max element = " + list.stream().mapToInt(n -> n).max().getAsInt());
        System.out.println("Min element = " + list.stream().mapToInt(n -> n).min().getAsInt());
        System.out.println("Sum = " + list.stream().mapToInt(n -> n).sum());
        System.out.println("Sum (using reduce) = " + list.stream().reduce((a, b) -> a + b).get());

        double average = list.stream().mapToDouble(n -> n).average().getAsDouble();
        System.out.println("Average element = " + average);

        List<Integer> subList = list.stream().mapToInt(n -> n).filter(n -> n > average).boxed().collect(Collectors.toList());
        System.out.println("Elements higher than " + average + ":" + subList
                + "\nThere are " + subList.stream().count() + " elements.");


    }
}
