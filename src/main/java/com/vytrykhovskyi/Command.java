package com.vytrykhovskyi;

@FunctionalInterface
public interface Command {
    void run(String string);
}
