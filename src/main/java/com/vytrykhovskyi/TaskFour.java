package com.vytrykhovskyi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class TaskFour {
    private List<String> text = new ArrayList<>();

    private TaskFour() {
        Scanner scanner = new Scanner(System.in);
        String line;
        System.out.println("Please, enter text:");
        while (!(line = scanner.nextLine()).equals("")) {
            this.text.add(line);

        }
    }

    private long getCountUniqueWords() {
        return text.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .distinct()
                .count();
    }

    private List<String> sortListOfUniqueWords() {
        return text.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    private Map<String, Long> getCountOfEachWord() {
        return text.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .collect(groupingBy(Function.identity(), counting()));
    }

    private Map<Object, Long> getCharsCount() {
        return text.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .flatMap(e -> e.chars().boxed())
                .filter(e -> e >= 97 && e <= 122)
                .map(e -> (char) e.intValue())
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }

    @Override
    public String toString() {
        return text + "\n";
    }

    public static void main(String[] args) {
        TaskFour taskFour = new TaskFour();

        System.out.println(taskFour);

        System.out.println("Count of unique words = " + taskFour.getCountUniqueWords());
        System.out.println("Sorted list of unique words = " + taskFour.sortListOfUniqueWords());
        System.out.println("Words frequency = " + taskFour.getCountOfEachWord());
        System.out.println("Chars frequency(without A-Z) = " + taskFour.getCharsCount());
    }
}
