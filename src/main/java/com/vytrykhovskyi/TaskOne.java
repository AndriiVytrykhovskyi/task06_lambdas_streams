package com.vytrykhovskyi;

public class TaskOne {
    public static void main(String[] args) {
        Setable max = (a, b, c) -> Math.max(a, Math.max(b, c));

        Setable average = (a, b, c) -> (a + b + c) / 3;

        System.out.println("Max value = " + max.getThreeNumbers(2, 8, 9));
        System.out.println("Average value = " + average.getThreeNumbers(3, 5, 6));
    }
}


