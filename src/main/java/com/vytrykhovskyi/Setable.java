package com.vytrykhovskyi;

@FunctionalInterface
public interface Setable {
    int getThreeNumbers(int one, int two, int three);
}
